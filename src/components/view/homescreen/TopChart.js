import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right } from 'native-base';
import axios from 'axios'
import { ScrollView } from 'react-native-gesture-handler';

export default class TopChart extends Component {

    // ft

    constructor(props) {
        super(props);
        this.state = {
            mc: []
        }
    }

    componentDidMount() {
        axios.get('https://reduxblog.herokuapp.com/api/posts?key=mc')
            .then(response => {
                this.setState({
                    mc: response.data
                })
            })
    }

    static navigationOptions = {
        title: 'MC'
    }

    render() {
        console.log('ini semua data', this.state.mc);
        
        return (
            <ScrollView>
                <View>
                    {
                        this.state.mc.map(mc =>
                            <View key={mc.id}>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('McDetail', mc)}
                                >
                                    <Card>
                                        <CardItem>
                                            <Left>
                                                <Body>
                                                    <Text style={{fontSize: 18}}>{mc.title}</Text>
                                                </Body>
                                            </Left>
                                        </CardItem>
                                        <CardItem cardBody>
                                            <Image source={{ uri: mc.categories }} style={{ height: 200, width: null, flex: 1 }} />
                                        </CardItem>
                                        <CardItem>
                                            <Body>
                                                <Button transparent>

                                                </Button>
                                            </Body>
                                        </CardItem>
                                    </Card>
                                </TouchableOpacity>
                            </View>
                        )
                    }
                </View>
            </ScrollView>
        )
    }
}

