import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';

export default class McDetail extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: navigation.state.params.title
    })

    render() {
        const { title, categories, content } = this.props.navigation.state.params
        return (
            <ScrollView>
                <View>
                    <Image
                        source={{ uri: categories }}
                        style={{ width: '100%', height: 200 }}
                    />
                    <View style={{padding: 20}}>
                        <Text style={{ 
                                fontSize: 18, 
                                textAlign: 'justify' 
                            }}
                        >
                            {content}
                        </Text>
                    </View>
                </View>
            </ScrollView>
        )
    }
}