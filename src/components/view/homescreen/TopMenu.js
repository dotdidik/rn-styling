import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/Ionicons'
class TopMenu extends Component {
    render() {
        return (
            <View>
                <View style={styles.container}>

                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('TopChart')}
                    >
                        <View style={styles.iconContain}>
                            <Icon 
                                name="ios-mic"
                                style={styles.icon}
                            />
                            <Text style={styles.textIcon}> MC </Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('FoodAndB')}
                    >
                        <View style={styles.iconContain}>
                            <Icon 
                                name="ios-beer"
                                style={styles.icon}
                            />
                            <Text style={styles.textIcon}> F & B </Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('TopChart')}
                    >
                        <View style={styles.iconContain}>
                            <Icon 
                                name="ios-color-wand"
                                style={styles.icon}
                            />
                            <Text style={styles.textIcon}> DECOR </Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}

export default withNavigation(TopMenu)

const styles = StyleSheet.create({
    container: {
        height: 110,
        marginLeft: 14,
        marginRight: 14,
        marginTop: 10,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconContain: {
        backgroundColor: '#889fbd',
        height: 87,
        margin: 15,
        width: 87,
        borderRadius: 10,
        justifyContent: "center",
        alignItems: "center",
    },
    icon: {
        fontSize: 50, 
        color: 'white'
    },
    textIcon: {
        color: 'white',
        fontWeight: 'bold'
    }
})