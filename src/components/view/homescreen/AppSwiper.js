
import React, { PureComponent } from 'react';
import { Text, Dimensions, Image, StyleSheet, View } from 'react-native';

import SwiperFlatList from 'react-native-swiper-flatlist';

export default class AppSwiper extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <SwiperFlatList
                    autoplay
                    autoplayDelay={5}
                    autoplayLoop
                    index={2}
                    showPagination
                >
                    <View style={[styles.child]}>
                        <Image
                            style={{ width: '100%', height: height * 0.3 }}
                            source={{ uri: 'https://estrilook.com/wp-content/uploads/2018/11/Desain-tanpa-judul-37.png' }}
                        />
                    </View>

                    <View style={[styles.child]}>
                        <Image
                            style={{ width: '100%', height: height * 0.3 }}
                            source={{ uri: 'https://www.nutritionaloutlook.com/sites/default/files/shutterstock_289498724_lowres.jpg' }}
                        />
                    </View>
                    
                    <View style={[styles.child]}>
                        <Image
                            style={{ width: '100%', height: height * 0.3 }}
                            source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT9DycVxu72QKmApuqfKjeBXtUWFINBrHSqMIIIiBg7Gkua471ZZA' }}
                        />
                    </View>

                </SwiperFlatList>
            </View>
        );
    }
}

export const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    child: {
        height: height * 0.3,
        width,
        justifyContent: 'center'
    },
    text: {
        fontSize: width * 0.5,
        textAlign: 'center'
    }
});