import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation'

class TownMenu extends Component {
    render() {
        return (
            <View>
                <View style={styles.cityContain}>
                    <Text style={styles.cityTitle}>CITY</Text>
                </View>
                <View style={styles.container}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('TopChart')}
                    >
                        <View style={styles.iconContain}>
                            <Image
                                style={{ width: '30%', height: 40 }}
                                source={{ uri: 'https://cdn4.iconfinder.com/data/icons/asian-capital-landmarks-1/92/ac_Jakarta-National_Monument-512.png' }}
                            />
                            <Text> Jakarta </Text>
                        </View>
                    </TouchableOpacity>

                    <View style={styles.iconContain}>
                        
                        <Image
                            style={{ width: '70%', height: 40 }}
                            source={{ uri: 'https://static.thenounproject.com/png/2326844-200.png' }}
                        />
                        <Text>Bandung</Text>
                    </View>

                    <View style={styles.iconContain}>
                        <Image
                            style={{ width: '50%', height: 40 }}
                            source={{ uri: 'https://cdn0.iconfinder.com/data/icons/landmark-color-sun-and-moon/100/Indonesia-512.png' }}
                        />
                        <Text>Yogyakarta</Text>
                    </View>

                </View>
            </View>
        )
    }
}

export default withNavigation(TownMenu)

const styles = StyleSheet.create({
    container: {
        height: 110,
        marginLeft: 14,
        marginRight: 14,
        marginTop: 4,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconContain: {
        backgroundColor: '#EDFAFD',
        height: 87,
        margin: 15,
        width: 87,
        borderRadius: 87 / 2,
        justifyContent: "center",
        alignItems: "center",
    },
    cityContain: { 
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    cityTitle: {
        fontSize: 20,
        fontWeight: 'bold'
    }
})