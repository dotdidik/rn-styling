import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import AppHeader from '../../common/AppHeader'
import TopMenu from './TopMenu';
import Recomendation from './Recomendation';
import CardHome from './CardHome';
import AppSwiper from './AppSwiper';

import Icon from 'react-native-vector-icons/Ionicons'
import HalfCard from '../../common/HalfCard';
import TownMenu from './TownMenu';

export default class HomeScreen extends Component {
  static navigationOptions = {
    title: 'Home',
  };
  render() {
    return (
      <View>
        <ScrollView>
            <AppSwiper />
            <TopMenu />
            <TownMenu />
        </ScrollView>
      </View>
    )
  }
}