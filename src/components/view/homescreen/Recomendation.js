import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'

export default class Recomendation extends Component {
    render() {
        return (
            <View style={Styles.container}>
                <View>
                    <Text>RECOMENDATION</Text>
                </View>
                <View style={Styles.cardContainer}>
                    <View style={Styles.tinnyCard}>
                        <Text>Card 1</Text>
                        <Image
                        style={{ width: '90%', height: 50}}
                        source={{uri: 'https://images.unsplash.com/photo-1517245480009-64056ebabb49?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}}
                    />
                        </View>
                    <View style={Styles.tinnyCard}>
                        <Text>Card 1</Text>
                        <Image
                        style={{ width: '90%', height: 50}}
                        source={{uri: 'https://images.unsplash.com/photo-1517245480009-64056ebabb49?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}}
                    />
                    </View>
                    <View style={Styles.tinnyCard}>
                        <Text>Card 1</Text>
                        <Image
                        style={{ width: '90%', height: 50}}
                        source={{uri: 'https://media-cdn.tripadvisor.com/media/photo-s/09/be/f4/db/centre-equestre-de-riambel.jpg'}}
                    />
                        </View>
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingLeft: 10
    },
    cardContainer:{
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center',
        margin: 5
    },
    tinnyCard: {
        width: '31%',
        backgroundColor: 'pink',
        margin: 5,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center'
    }

})
