import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Card from '../../common/Card'

export default class CardHome extends Component {
    state = {
        carddata: [
            {
                title: 'Log In',
                subtitle: 'Menu',
                image:
                    'https://png.pngtree.com/png-vector/20190130/ourmid/pngtree-hand-drawn-cartoon-tree-material-treecartoon-planthand-drawn-png-image_604847.jpg'
            },
            {
                title: 'Log In',
                subtitle: 'Menu',
                image:
                    'https://png.pngtree.com/png-vector/20190130/ourmid/pngtree-hand-drawn-cartoon-tree-material-treecartoon-planthand-drawn-png-image_604847.jpg'
            },
            {
                title: 'Log In',
                subtitle: 'Menu',
                image:
                    'https://png.pngtree.com/png-vector/20190130/ourmid/pngtree-hand-drawn-cartoon-tree-material-treecartoon-planthand-drawn-png-image_604847.jpg'
            }
        ]
    }
    render() {
        return (
            <View style={Styles.container}>
                <View>
                    <Text>RECOMENDATION</Text>
                </View>
                <View>
                    {
                        this.state.carddata.map((ini, index) =>
                            <View key={index}>
                                <Card
                                    title={ini.title}
                                    image={ini.image}
                                    subtitle={ini.subtitle}
                                />
                            </View>
                        )
                    }
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        paddingTop: 10,
        paddingLeft: 10
    }

})
