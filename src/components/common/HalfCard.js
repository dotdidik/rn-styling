import React, { Component } from 'react'
import { Text, View, StyleSheet, ImageBackground } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';

import axios from 'axios';

export default class HalfCard extends Component {
    constructor(props){
        super(props);
        this.state = {
            movies: []
        }
    }

    componentDidMount(){
        axios.get('https://api.themoviedb.org/3/trending/all/day?api_key=9824a5ecced9b0e31bda819babb795b0')
        .then(response => {
            this.setState({
                movies: response.data.results
            })
        })
    }

    render() {
        console.log('data', this.state.movies);
        
        return (
            <View>
                <View style={Styles.titleView}>
                    <Text style={Styles.title}> BOX OFFICE </Text>
                </View>
                <View style={Styles.scrollContainer}>
                    <ScrollView horizontal={true}>
                        {
                            this.state.movies.map((movie, index)=>
                                <View key={index} style={Styles.cardImage}>
                                    <ImageBackground source={{
                                        uri: `https://image.tmdb.org/t/p/w500/${movie.poster_path}`
                                    }}
                                        style={{ height: '100%', height: '100%' }}
                                    >

                                        <Text style={Styles.cardTitle}>{movie.original_title}</Text>
                                    
                                    </ImageBackground>
                                </View>
                            )
                        }
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    titleView: {
        padding: 10
    },
    title: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    scrollContainer: {
        padding: 10
    },
    cardImage: {
        width: 200,
        height: 300,
        backgroundColor: '#fff',
        margin: 10
    },
    cardTitle: {
        position: 'absolute', 
        left: 0, 
        right: 0, 
        bottom: 0, 
        justifyContent: 'center',
        alignItems: 'center', 
        backgroundColor: 'rgba(205, 223, 221, 0.71)',
        height: 30,
        color: 'white',
        fontWeight: 'bold'
    }
})