import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

class AppHeader extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}> Awesome </Text>
            </View>
        )
    }
}

export default AppHeader;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#34ebdb',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 25,
        color: 'white',
        fontWeight: 'bold'
    }
})
