import React, { Component } from 'react'
import { Text, View,Image ,StyleSheet } from 'react-native'

class Card extends Component {
    render() {
        return (
            <View style={styles.CardContainer}>
                <Image
                    style={styles.ImageStyle}
                    source={{uri: this.props.image}}
                />
                <Text style={styles.TextTitle}>{this.props.title}</Text>
                <Text style={styles.TextSubTitle}>{this.props.subtitle}</Text>
            </View>
        )
    }
}

export default Card

const styles = StyleSheet.create({
    CardContainer:{
        borderColor:'#fff',
        borderWidth: 5,
        backgroundColor: '#acc3e8'
    },

    ImageStyle:{
        width: '94%', 
        height: 120,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10
    },

    TextTitle: {
        fontSize:25,
        fontWeight: 'bold',
        paddingLeft:10
    },

    TextSubTitle: {
        fontSize:18,
        fontWeight: '400',
        paddingLeft:10
    }
})
