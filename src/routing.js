import React from 'react';
import { View, Text, Button } from 'react-native'
import HomeScreen from './components/view/homescreen/HomeScreen'
import Profile from './components/view/profile/Profile'
import ProductsScreen from './components/view/setting/Products'

import Icon from 'react-native-vector-icons/Ionicons'

import {
    createBottomTabNavigator,
    createStackNavigator,
    createAppContainer,
} from 'react-navigation';

import TopChart from './components/view/homescreen/TopChart';
import FoodAndB from './components/view/homescreen/FoodAndB'
import McDetail from './components/view/homescreen/McDetail';


class DetailsScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Details!</Text>
            </View>
        );
    }
}


const HomeStack = createStackNavigator({
    Home: HomeScreen,
    FoodAndB: FoodAndB,
    TopChart: TopChart,
    McDetail: McDetail
})

HomeStack.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarOptions: { activeTintColor: 'red', },
    tabBarIcon: ({ tintColor, focused }) => (
        <View>
        <Icon
            style={focused ? [{ color: 'red' }] : [{ color: tintColor }]}
            size={20} name={'ios-alarm'}
        />
        </View>)
}


const ProductStack = createStackNavigator({
    Products: ProductsScreen,
    Details: DetailsScreen,
});

const ProfileStack = createStackNavigator({
    Profile: Profile,
    Details: DetailsScreen
})

const TabNavigator = createAppContainer(createBottomTabNavigator(
    {
        Home: HomeStack,
        Products: ProductStack,
        Profile: ProfileStack
    }
));

export default TabNavigator
