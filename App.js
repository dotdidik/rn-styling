import React, { Component } from 'react'
import { Text, View } from 'react-native'
import TabNavigator from './src/routing'

export default class App extends Component {
  render() {
    return (
        <TabNavigator />
    )
  }
}
